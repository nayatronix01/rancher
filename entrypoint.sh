#!/bin/bash
#sed -i 's/localhost:6443/k3s-server:6443/' /kubeconfig.yaml
export KUBECONFIG=/kubeconfig.yaml
helm init --client-only --skip-refresh
helm repo rm stable
helm repo add stable https://charts.helm.sh/stable
helm repo add rancher-latest https://releases.rancher.com/server-charts/latest
helm repo update
helm plugin install https://github.com/rimusz/helm-tiller
helmfile apply --values values-prod.yaml
#helmfile apply 2>&1
