resource "docker_container" "helmfile" {
  name       = "terraform-helmfile"
  image      = "quay.io/roboll/helmfile:v0.80.2"
  #links      = ["k3s-server"]
  entrypoint = ["/entrypoint.sh"]
  start      = true


  upload  {
    content    = "${data.template_file.entrypoint.rendered}"
    file       = "/entrypoint.sh"
    executable = true
   }
 
  upload  {
    content = "${data.template_file.kubeconfig.rendered}"
    file    = "/kubeconfig.yaml"
  }
 
  upload  {
    content = "${data.template_file.helmfile.rendered}"
    file    = "/helmfile.yaml"
  }

  
  upload  {
    content = "${data.template_file.values-prod.rendered}"
    file    = "/values-prod.yaml"
  }


  depends_on = [
    null_resource.dockerrm,
  ]
}

resource "null_resource" "dockerrm" {
    provisioner "local-exec" {
      command = "docker rm -f terraform-helmfile"
      when = destroy
    }
}



#resource "helmfile_release_set" "rancher" {
#    content = file("./helmfile.yaml")
#    working_directory = "./"
#    kubeconfig        = pathexpand("./kubeconfig.yaml")
#    #environment       = "prod"
#}

#data "terraform_remote_state" "rancher" {
#  backend = "consul"
#  config = {
#    address = "consul-server.vmware.nayatronix.com"
#    access_token ="c179d912-5d90-4671-c483-7a3b5a4c2e70"
#    path = "terraform-remote-state/kube-prometheus-stack"
#  }
#}


#data "kubernetes_namespace" "rancher" {
#  metadata {
#    name = "cattle-system"
#  }
#}

